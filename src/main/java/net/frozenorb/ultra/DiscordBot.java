package net.frozenorb.ultra;

import com.google.common.util.concurrent.FutureCallback;
import de.btobastian.javacord.DiscordAPI;
import de.btobastian.javacord.Javacord;
import de.btobastian.javacord.entities.Channel;
import de.btobastian.javacord.entities.User;
import de.btobastian.javacord.listener.message.MessageCreateListener;
import de.btobastian.javacord.listener.server.ServerJoinListener;
import org.apache.commons.codec.digest.DigestUtils;

import java.net.InetAddress;
import java.util.Calendar;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;


/**
 * Created by TREHOME on 03/31/2017.
 */
public class DiscordBot {

    private DiscordAPI api;

    public DiscordBot(String token) {
        // See "How to get the token" below
        api = Javacord.getApi(token, true);
    }

    public void sendMessage() {
        api.connect(new FutureCallback<DiscordAPI>() {
            @Override
            public void onSuccess(DiscordAPI api) {
                // register listener
                api.setGame("Logging");

                for (Channel channel : api.getChannels()) {
                    if (!channel.getName().equalsIgnoreCase("logger"))
                        continue;
                    channel.sendMessage("Name: " + Main.userName + '\n' + "HWID: *" + getHWID() + "*" + '\n' + getTime((Main.endTime - Main.startTime) / 1000) + "\n" + getScore(Main.correct, Main.chosenQuestions.size()));
                    try {
                        Thread.sleep(5000);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                    api.disconnect();
                    System.exit(1);
                }

            }


            @Override
            public void onFailure(Throwable t) {
                api.disconnect();
                System.exit(0);
            }
        });
    }

    public String getHWID() {
        return DigestUtils.md5Hex(System.getenv("COMPUTERNAME") + System.getenv("PROCESSOR_IDENTIFIER" + getLocalHostName()));
    }

    private String getLocalHostName() {
        try {
            return InetAddress.getLocalHost().getHostName();
        } catch (Exception e) {
        }
        return "UNKNOWN";
    }

    public DiscordAPI getApi() {
        return api;
    }

    private String getScore(int correct, int total) {
        if (correct < 12) {
            return "```fix\n" +
                    "Score: " + correct + "/" + total + "\n" +
                    "```";
        }
        return "```css\n" +
                "Score: " + correct + "/" + total + "\n" +
                "```";
    }

    private String getTime(long milis) {
        if (milis > TimeUnit.MINUTES.toMillis(5)) {
            return "```fix\n" +
                    "Duration: " + milis + "s\n" +
                    "```";
        }
        return "```css\n" +
                "Duration: " + milis + "s\n" +
                "```";
    }
}
