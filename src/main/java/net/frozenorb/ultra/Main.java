package net.frozenorb.ultra;

import net.frozenorb.ultra.frame.NameFrame;

import java.util.*;

/**
 * Created by TREHOME on 04/08/2017.
 */
public class Main {


    public static final int WINDOW_WIDTH = 500;
    public static final int WINDOW_HEIGHT = 300;
    public static int QUESTIONS;
    public static List<Question> questions = new ArrayList<>();
    public static long startTime;
    public static String userName;
    public static Set<Question> chosenQuestions = new HashSet<>();
    public static int correct = 0;
    public static DiscordBot discordBot;
    public static long endTime;

    public static void main(String [] args)
    {
        questions.add(new Question("How long do you ban for if you catch them using a client in an SS?", "Blacklist", Arrays.asList("Blacklist", "1 month", "Permanently", "6 months")));
        questions.add(new Question("When you open their .miencraft folder, what's the first thing you do?", "Toggle View Hidden Files", Arrays.asList("Toggle View Hidden Files", "Go to their mods", "Go to their downloads", "Open their private documents")));
        questions.add(new Question("What's a good strategy to use when screensharing?", "All of these are good", Arrays.asList("All of these are good", "Check their logs folder", "Check their config folder for X-Ray", "Getting them to admit")));
        questions.add(new Question("What do you if someone asks you what methods you're using?", "Don't tell them anything", Arrays.asList("Don't tell them anything", "Tell them", "Hint towards it", "Ban them")));
        questions.add(new Question("What do you do if you suspect someone of cheating but can't find it, and a more experienced SS'er is online?", "Ask someone more experienced to SS them", Arrays.asList("Ask someone more experienced to SS them", "Leave them", "Ban them anyways", "Force them to admit")));
        questions.add(new Question("Are you capable of going to the next question?", "YES!!!!!", Arrays.asList("YES!!!!!", "No I'm bad", "Maybe... ;)", "demote me")));
        questions.add(new Question("What would you do if they were trolling you during the SS", "Blacklist", Arrays.asList("Blacklist", "Permanently ban them", "Play along with them", "Troll them back")));
        questions.add(new Question("How long do you ban for if they refuse to screenshare?", "Permanently", Arrays.asList("Permanently", "1 month", "Don't ban them", "6 months")));
        questions.add(new Question("If you find something suspcious and you don't know what it is, what do you do about it?", "Ask someone like Vykz about it", Arrays.asList("Ask someone like Vykz about it", "Ban them anyways", "Tell them to send it to you so you can use it", "Leave them")));
        questions.add(new Question("If they don't admit in the beginning, but admit after you've found their cheats, what do you do?", "Blacklist", Arrays.asList("Blacklist", "Permanently ban", "Forgive them", "6 month ban")));
        questions.add(new Question("What are the 2 screensharing programs we allow you to use?", "join.me and AnyDesk", Arrays.asList("join.me and AnyDesk", "Java and C#", "TeamSpeak and Skype", "Discord and Curse")));
        questions.add(new Question("If they make an excuse saying they crashed or logged out while frozen, what do you do?", "1 month ban", Arrays.asList("1 month ban", "Permanently ban", "2 week ban", "Forgive them")));
        questions.add(new Question("How do you navigate to their minecraft folder?", "In game through resourcepacks", Arrays.asList("In game through resourcepacks", "Ask them to do it for you", "Through the computer XDD!!", "%appdata%")));
        questions.add(new Question("Do you ban them if their mods are a couple KB off?", "No, each version differentiates by a couple sizes", Arrays.asList("No, each version differentiates by a couple sizes", "Yes ban them!!!!", "Don't do anything", "If it's 1 KB off, BAN THEM!!!!")));
        questions.add(new Question("Are you satisfied with Ultra and its policies?", "Yes, and I support it fully", Arrays.asList("Yes, and I support it fully", "No they're shit policies", "No, I don't like them", "i like pie")));
        QUESTIONS = questions.size();
        startTime = System.currentTimeMillis();
        new NameFrame();
        discordBot = new DiscordBot("MzE0ODk0NTkwMDA2MjYzODE5.C__XmQ.iQb09j3sKcsJTYuxIzkAcfm-2_M");
    }
}
