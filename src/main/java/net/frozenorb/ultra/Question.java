package net.frozenorb.ultra;

import java.util.List;
import java.util.UUID;

public class Question {

    private String question;
    private String answer;
    private List<String> questions;
    private UUID uuid;

    public Question(String question, String answer, List<String> questions) {
        this.question = question;
        this.answer = answer;
        this.questions = questions;
        uuid = UUID.randomUUID();
    }

    public String getQuestion() {
        return question;
    }

    public String getAnswer() {
        return answer;
    }

    public List<String> getQuestions() {
        return questions;
    }

    public UUID getUuid() {
        return uuid;
    }
}
