package net.frozenorb.ultra.frame;

import net.frozenorb.ultra.Main;

import javax.swing.*;
import java.awt.event.*;

public class NameFrame extends JFrame {

    public NameFrame() {
        setTitle("Enter your ingame name!");

        setResizable(false);
        setLocationRelativeTo(null);

        setSize(Main.WINDOW_WIDTH, Main.WINDOW_HEIGHT);

        setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);

        JLabel messageLabel = new JLabel("Please enter your minecraft IGN, after please press enter.");

        JTextField area = new JTextField("IGN Here");
        area.addKeyListener(new KeyAdapter() {

            @Override
            public void keyPressed(KeyEvent e) {
                if (e.getKeyChar() != KeyEvent.VK_ENTER)
                    return;

                String text = area.getText();
                if (text == null || text.equalsIgnoreCase("IGN Here") || text.length() < 3 || text.length() > 16)
                    return;

                Main.userName = text;
                Main.startTime = System.currentTimeMillis();
                new QuestionFrame();
                dispose();
            }
        });

        JPanel panel = new JPanel();
        panel.add(messageLabel);
        panel.add(area);

        add(panel);

        setVisible(true);
    }
}
