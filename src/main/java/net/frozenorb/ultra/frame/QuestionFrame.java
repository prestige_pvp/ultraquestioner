package net.frozenorb.ultra.frame;

import de.btobastian.javacord.DiscordAPI;
import net.frozenorb.ultra.DiscordBot;
import net.frozenorb.ultra.Main;
import net.frozenorb.ultra.Question;

import javax.swing.*;
import java.awt.event.*;
import java.util.*;
import java.util.List;

public class QuestionFrame extends JFrame {


    public QuestionFrame() {
        setTitle("Question: " + (Main.chosenQuestions.size()) + '/' + Main.QUESTIONS);

        setSize(Main.WINDOW_WIDTH, Main.WINDOW_HEIGHT);


        setAlwaysOnTop(true);
        setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);

        Main.questions.removeAll(Main.chosenQuestions);
        if (Main.questions.size() == 0) {
            JOptionPane.showMessageDialog(null, "You got: " + Main.correct + " questions correct!");
            Main.endTime = System.currentTimeMillis();
            dispose();
            Main.discordBot.sendMessage();
            return;
        }
        List<Question> remaining = Main.questions;
        Collections.shuffle(remaining);
        Question question = remaining.get(0);

        JLabel messageLabel = new JLabel(question.getQuestion());

        List<String> questions = question.getQuestions();
        Collections.shuffle(questions);
        List<JButton> jButtons = new ArrayList<>();

        for (String questionString : questions) {
            JButton jButton = new JButton(questionString);
            jButton.addActionListener(new ButtonListener(question.getAnswer()));
            jButtons.add(jButton);
        }


        JPanel panel = new JPanel();
        panel.add(messageLabel);
        for (JButton jButton : jButtons) {
            panel.add(jButton);
        }
        Main.chosenQuestions.add(question);
        add(panel);

        setVisible(true);
    }


    private class ButtonListener implements ActionListener {
        private String answer;

        public ButtonListener(String answer) {
            this.answer = answer;
        }

        public void actionPerformed(ActionEvent e) {

            String actionCommand = e.getActionCommand();

            if (actionCommand.equals(answer)) {
                Main.correct++;
            }
                dispose();
                new QuestionFrame();
        }

    }

}
